'use strict'

let number = prompt("Enter number", "")

function factorial(x) {
    if (x < 0){
        return 0;
    } else if (x == 0) {
        return 1;
    } else return x * factorial(x - 1);
}

console.log(factorial(number));